const kerucut = require("./module/kerucut.js");
const balok = require("./module/balok");
const kubus = require("./module/kubus");
const tabung = require("./module/tabung");
const lingkaran = require("./module/lingkaran");
const persegi = require("./module/persegi");
const persegiPanjang = require("./module/persegiPanjang");

class Module {
	constructor() {
		this.name = "Menghitung Bangun Ruang";
	}

	menghitungVolumeKerucut(radius, sisi, tinggi) {
		let v = new kerucut(radius, sisi, tinggi);
		return v.menghitungVolume();
	}

	menghitungVolumeBalok(panjang, tinggi, lebar) {
		let v = new balok(panjang, tinggi, lebar);
		return v.menghitungVolume();
	}

	menghitungVolumeKubus(sisi) {
		let v = new kubus(sisi);
		return v.menghitungVolume();
	}

	menghitungVolumeTabung(radius, tinggi) {
		let v = new tabung(radius, tinggi);
		return v.menghitungVolume();
	}

	menghitungLuasPersegi(sisi) {
		let l = new persegi(sisi);
		return l.menghitungLuas();
	}

	menghitungLuasPersegiPanjang(panjang, lebar) {
		let l = new persegiPanjang(panjang, lebar);
		return l.menghitungLuas();
	}

	menghitungLuasLingkaran(radius) {
		let l = new lingkaran(radius);
		return l.menghitungLuas();
	}

	menghitungKelilingPersegi(sisi) {
		let l = new persegi(sisi);
		return l.menghitungKeliling(indexControllers);
	}

	menghitungKelilingPersegiPanjang(panjang, lebar) {
		let l = new persegiPanjang(panjang, lebar);
		return l.menghitungKeliling();
	}

	menghitungKelilingLingkaran(radius) {
		let l = new lingkaran(radius);
		return l.menghitungKeliling();
	}
}
/* End class Module */

module.exports = Module;
