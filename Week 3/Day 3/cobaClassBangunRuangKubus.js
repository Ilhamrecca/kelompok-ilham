const all = require("./module.js");
const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

/* Start Kubus */
let main = new all();
function menu() {
	console.clear();
	console.log("Menu");
	console.log("====");
	console.log("1. Kubus");
	console.log("2. Tabung");
	console.log("3. Balok");
	console.log("4. Kerucut");
	console.log("5. Persegi");
	console.log("6. Persegi Panjang");
	console.log("7. Lingkaran");
	console.log("8. Exit");

	rl.question("Choose option: ", (option) => {
		switch (option) {
			case "1":
				rl.question("Masukan sisi : ", (sisi) => {
					console.log(main.menghitungVolumeKubus(sisi));
					rl.question("Press enter to continue....", () => {
						menu();
					});
				});

				break;
			case "2":
				rl.question("Masukan Tinggi : ", (tinggi) => {
					rl.question("Masukan Radius : ", (radius) => {
						console.log(
							main.menghitungVolumeTabung(tinggi, radius)
						);
						rl.question("Press enter to continue....", () => {
							menu();
						});
					});
				});

				break;
			case "3":
				rl.question("Masukan Panjang : ", (panjang) => {
					rl.question("Masukan Lebar : ", (lebar) => {
						rl.question("Masukan Tinggi : ", (tinggi) => {
							console.log(
								main.menghitungVolumeBalok(
									panjang,
									lebar,
									tinggi
								)
							);
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					});
				});
				break;
			case "4":
				rl.question("Masukan Radius : ", (radius) => {
					rl.question("Masukan Sisi : ", (sisi) => {
						rl.question("Masukan Tinggi : ", (tinggi) => {
							console.log(
								main.menghitungVolumeKerucut(
									tinggi,
									sisi,
									tinggi
								)
							);
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					});
				});
				break;
			case "5":
				rl.question("Choose :\n1. Luas \n2. Keliling \n : ", (a) => {
					if (a == "1") {
						rl.question("Masukan Sisi : ", (sisi) => {
							console.log(main.menghitungLuasPersegi(sisi));
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					} else if (a == 2) {
						rl.question("Masukan Sisi : ", (sisi) => {
							console.log(main.menghitungKelilingPersegi(sisi));
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					}
				});

				break;
			case "6":
				rl.question("Choose :\n1. Luas \n2. Keliling \n : ", (a) => {
					if (a == "1") {
						rl.question("Masukan Panjang : ", (panjang) => {
							rl.question("Masukan Lebar : ", (lebar) => {
								console.log(
									main.menghitungLuasPersegiPanjang(
										panjang,
										lebar
									)
								);
								rl.question(
									"Press enter to continue....",
									() => {
										menu();
									}
								);
							});
						});
					} else if (a == 2) {
						rl.question("Masukan Panjang : ", (panjang) => {
							rl.question("Masukan Lebar : ", (lebar) => {
								console.log(
									main.menghitungKelilingPersegiPanjang(
										panjang,
										lebar
									)
								);
								rl.question(
									"Press enter to continue....",
									() => {
										menu();
									}
								);
							});
						});
					}
				});

				break;
			case "7":
				rl.question("Choose :\n1. Luas \n2. Keliling \n : ", (a) => {
					if (a == "1") {
						rl.question("Masukan radius : ", (radius) => {
							console.log(main.menghitungLuasLingkaran(radius));
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					} else if (a == 2) {
						rl.question("Masukan radius : ", (radius) => {
							console.log(
								main.menghitungKelilingLingkaran(radius)
							);
							rl.question("Press enter to continue....", () => {
								menu();
							});
						});
					}
				});

				break;
			case "8":
				process.exit();
				break;
			default:
				menu();
		}
	});
}

rl.on("close", () => {
	process.exit();
});

const choice = function () {
	rl.question("Choose :\n1. Luas \n2. Keliling \n : ", (choose) => {});
	return choose;
};

menu();
