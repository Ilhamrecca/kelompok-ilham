// Import BangunRuang class
const bangunRuang = require("./bangunruang.js");

class Balok extends bangunRuang {
	constructor(panjang, tinggi, lebar) {
		super("Persegi");
		this.panjang = panjang;
		this.tinggi = tinggi;
		this.lebar = lebar;
	}

	menghitungVolume() {
		return this.panjang * this.lebar * this.tinggi;
	}
}
/* End Kubus class */

module.exports = Balok;
