// import parent class
const BangunRuang = require("./bangunruang.js");

class Kerucut extends BangunRuang {
	constructor(radius, sisi, tinggi) {
		super("Kerucut");

		this.radius = radius;
		this.sisi = sisi;
		this.tinggi = tinggi;
	}

	// Override menghitungVolume
	menghitungVolume() {
		return (1 / 3) * Math.PI * Math.pow(this.radius, 2) * this.tinggi;
	}
}

module.exports = Kerucut;
