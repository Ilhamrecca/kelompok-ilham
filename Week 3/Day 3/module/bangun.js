/* Make BangunRuang Abstract Class */
class Bangun {
	// Make constructor with name variable/property
	constructor(name) {
		if (this.constructor === Bangun) {
			throw new Error("This is abstract!");
		}

		this.name = name;
	}

	getName() {
		return this.name;
	}
}
/* End BangunRuang Abstract Class */

module.exports = Bangun;
