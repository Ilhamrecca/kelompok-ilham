// Import BangunRuang class
const bangunDatar = require("./bangunDatar.js");

/* Make Kubus class that is parent of BangunRuang class (This is inheritance) */
class persegiPanjang extends bangunDatar {
	// Make constructor with sisi of persegi
	constructor(panjang, lebar) {
		super("Persegi Panjang");
		this.panjang = panjang;
		this.lebar = lebar;
	}

	menghitungKeliling() {
		return this.panjang * 2 + this.lebar * 2;
	}
	menghitungLuas() {
		return this.panjang * this.lebar;
	}
}
/* End Kubus class */

module.exports = persegiPanjang;
