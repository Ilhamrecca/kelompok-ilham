// Import BangunRuang class
const bangunDatar = require("./bangunDatar.js");

/* Make Kubus class that is parent of BangunRuang class (This is inheritance) */
class Lingkaran extends bangunDatar {
	// Make constructor with sisi of persegi
	constructor(radius) {
		super("Lingkaran");
		this.radius = radius;
	}

	// Overriding menghitungLuas from BangunDatar class
	menghitungLuas() {
		return this.radius ** 2 * 3.14;
	}

	menghitungKeliling() {
		return 3.14 * 2 * this.radius;
	}
}
/* End Kubus class */

module.exports = Lingkaran;
