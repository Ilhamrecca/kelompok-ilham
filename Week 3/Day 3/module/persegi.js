// Import BangunRuang class
const bangunDatar = require("./bangunDatar.js");

/* Make Kubus class that is parent of BangunRuang class (This is inheritance) */
class Persegi extends bangunDatar {
	// Make constructor with sisi of persegi
	constructor(sisi) {
		super("Persegi");
		this.sisi = sisi;
	}

	// Overriding menghitungLuas from BangunDatar class
	menghitungLuas() {
		return this.sisi ** 2;
	}

	menghitungKeliling() {
		return this.sisi * 4;
	}
}
/* End Kubus class */

module.exports = Persegi;
