// Import BangunRuang class
const bangunRuang = require("./bangunruang.js");

/* Make Kubus class that is parent of BangunRuang class (This is inheritance) */
class Kubus extends bangunRuang {
	// Make constructor with sisi of persegi
	constructor(sisi) {
		super("Kubus");
		this.sisi = sisi;
	}

	menghitungVolume() {
		return this.sisi ** 3;
	}
}
/* End Kubus class */

module.exports = Kubus;
