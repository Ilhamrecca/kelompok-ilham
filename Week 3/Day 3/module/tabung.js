const BangunRuang = require("./bangunruang.js");

class Tabung extends BangunRuang {
	constructor(radius, tinggi) {
		super("Tabung");
		this.radius = radius;
		this.tinggi = tinggi;
	}

	menghitungVolume() {
		return Math.PI * Math.pow(this.radius, 2) * this.tinggi;
	}
}

module.exports = Tabung;
