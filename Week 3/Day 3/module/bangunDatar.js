const bangun = require("./bangun");

/* Make BangunRuang Abstract Class */
class bangunDatar extends bangun {
	// Make constructor with name variable/property
	constructor(name) {
		super(name);
		if (this.constructor === bangunDatar) {
			throw new Error("This is abstract!");
		}
		this.name = name;
	}

	// menghitungLuas instance method
	menghitungLuas() {}

	// menghitungKeliling instance method
	menghitungKeliling() {}
}
/* End BangunRuang Abstract Class */

module.exports = bangunDatar;
