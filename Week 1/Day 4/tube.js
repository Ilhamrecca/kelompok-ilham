const index = require("./question.js");
function tube() {
	console.clear();
	console.log(`TUBE'S VOLUME`);
	console.log(`=========`);
	input();
}

function input() {
	index.question("What's the side Radius of the Tube ? ", (radius) => {
		index.question("Whats the height of the Tube ", (height) => {
			if (!isNaN(radius) && !isEmptyOrSpaces(radius)) {
				calculateTube(radius, height);
				index.close();
			} else {
				console.log(`Radius must be a number\n`);
				input();
			}
		});
	});
}

function calculateTube(radius, height) {
	console.log(
		`\n\nThe Volume of the Tube is ${
			3.14 * radius ** 2 * height
		} Unit Volume`
	);
}

function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === "0";
}

module.exports.tube = tube;
