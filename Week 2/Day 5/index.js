const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

// Optional
function clean(data) {
	return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscendingBubble(data) {
	// Code Here
	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length - i; j++) {
			if (data[j - 1] > data[j]) {
				let store = data[j - 1];
				data[j - 1] = data[j];
				data[j] = store;
			}
		}
	}
	return data;
}

// Should return array
function sortDescendingBubble(data) {
	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length - i; j++) {
			if (data[j - 1] < data[j]) {
				let store = data[j - 1];
				data[j - 1] = data[j];
				data[j] = store;
			}
		}
	}
	return data;
}

const sortAscendingInsertion = function (data) {
	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length; j++) {
			if (data[i] < data[j]) {
				let store = data[i];
				data[i] = data[j];
				data[j] = store;
			}
		}
	}
	return data;
};

const sortDescendingInsertion = function (data) {
	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length; j++) {
			if (data[i] > data[j]) {
				let store = data[i];
				data[i] = data[j];
				data[j] = store;
			}
		}
	}
	return data;
};

const start = function () {
	let sortAscending;
	let sortDescending;
	let newData = clean(data);

	rl.question(
		"What sorting method would you like to use ? \n 1. Bubble Sort \n 2. Insertion Sort\n Select either 1 or 2 or neither to exit : ",
		(choice) => {
			switch (choice) {
				case "1":
					sortAscending = sortAscendingBubble;
					sortDescending = sortDescendingBubble;
					testIt(sortAscending, sortDescending, newData);
					break;
				case "2":
					sortAscending = sortAscendingInsertion;
					sortDescending = sortDescendingInsertion;
					testIt(sortAscending, sortDescending, newData);
					break;
				default:
					rl.close();
					console.log("");
			}
		}
	);
};

const testIt = function (sortAscending, sortDescending, newData) {
	test(sortAscending, sortDescending, newData);
	rl.question("Press Enter to Try Again....", () => {
		console.clear();
		start();
	});
};

rl.on("close", () => {
	process.exit();
});

start();

// DON'T CHANGE

// let newData = clean(data);
// console.log(sortAscending(newData));
// console.log(sortDecending(newData));
