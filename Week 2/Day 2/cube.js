const index = require("./question.js");

function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === "0";
}

function calculateCube(length) {
	return length ** 3;
}

function input() {
	index.question("length: ", (length) => {
		if (!isNaN(length) && !isEmptyOrSpaces(length)) {
			console.log(`Cube volume is ${calculateCube(length)}\n`);
			index.close();
		} else {
			console.log("Length must be number \n");
			input();
		}
	});
}

module.exports.input = input;
