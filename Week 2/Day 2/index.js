const input = require("./question.js");
const ball = require("./ball.js");
const cone = require("./cone.js");
const cube = require("./cube.js");
const tube = require("./tube.js");

function menu() {
	console.log("Menu");
	console.log("====");
	console.log("1. Cube");
	console.log("2. Cone");
	console.log("3. Ball");
	console.log("4. Tube");
	console.log("5. Exit");
	input.question("Choose option: ", (option) => {
		// if (option == 1) {
		// 	cone.inputRadius();
		// } else if (option == 2) {
		// 	cube.input();
		// } else if (option == 3) {
		// 	rl.close();
		// } else {
		// 	menu();
		// }
		switch (option) {
			case "1":
				cube.input();
				break;
			case "2":
				cone.cone();
				break;
			case "3":
				ball.ball();
				break;
			case "4":
				tube.tube();
				break;
			case "5":
				rl.close;
				break;
			default:
				menu();
		}
	});
}

menu();
// module.exports.input = { question, rl, menu };
