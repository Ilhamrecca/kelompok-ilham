const connection = require('../models/connection.js')

class PelangganController {

// Function getAll pelanggan table
async getAll(req, res) {
  try {
      var sql = "SELECT * FROM pelanggan"
      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne pelanggan table
  async getOne(req, res) {
    try {
      var sql = "SELECT * FROM pelanggan WHERE id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

// Create data
  async create(req, res) {
    try {
      var sqlInsert = 'INSERT INTO pelanggan(nama) VALUES (?)'

      connection.query(sqlInsert, [req.body.nama], (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          })
        }

        var sqlSelect = "SELECT * FROM pelanggan where id = ?"

        connection.query(sqlSelect, [result.insertId], function(err, result) {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } 

          // If success it will return JSON of result
          res.json({
            status: 'Success add data',
            data: result[0]
          })
        })
      })
    }
    catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

// Update data
  async update(req, res) {
    try {
      var sqlUpdate = 'UPDATE pelanggan SET nama = ? WHERE id = ?'

      connection.query(sqlUpdate, [req.body.nama, req.params.id], (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          })
        }

        var sqlSelect = "SELECT * FROM pelanggan WHERE id = ?"

        connection.query(sqlSelect, [req.params.id], function(err, result) {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } res.json({
            status: 'Success update data id ' + [req.params.id],
            data: result[0]
          })
        })
      })
   }
   catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

// Delete data
  async delete(req, res) {
    try {

      var sqlDelete = 'DELETE FROM pelanggan WHERE id = ?'

      connection.query(sqlDelete, [req.params.id], (err, result) => {
        if (err){
          res.json({
            status: "Error",
            error: err
          })
        }
        res.json({
          status: 'Success delete data id ' + [req.params.id]
        })
      })
    }
    catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
}


module.exports = new PelangganController