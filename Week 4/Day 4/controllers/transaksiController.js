const connection = require('../models/connection.js')

class TransaksiController {

// Function getAll transaksi table
async getAll(req, res) {
  try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Create data
  async create(req, res) {
    try {

      // GET harga From barang table
      var sql = 'SELECT harga FROM barang where id = ?'

      // SELECT harga Query
      connection.query(sql, [req.body.id_barang], function(err, result) {
        if (err) {
          res.json({
            status: "Error",
            error: err
          });
        } // If error

        // If success it will make total variable
      var total = result[0].harga * req.body.jumlah

      var sqlInsert = 'INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)'

      connection.query(sqlInsert, [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total], (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          })
        }

        var sqlSelect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" 

        connection.query(sqlSelect, [result.insertId], function(err, result) {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } 

          // If success it will return JSON of result
          res.json({
            status: 'Success add data',
            data: result[0]
          })
        })
      })
    })
    }
    catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

  // Update data
  async update(req, res) {
    try {
      // GET harga From barang table
      var sql = 'SELECT harga FROM barang where id = ?'

      // SELECT harga Query
      connection.query(sql, [req.body.id_barang], function(err, result) {
        if (err) {
          res.json({
            status: "Error",
            error: err
          });
        } // If error

        // If success it will make total variable
      var total = result[0].harga * req.body.jumlah

      var sqlUpdate = 'UPDATE transaksi t SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ? WHERE id = ?'

      connection.query(sqlUpdate, [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total, req.params.id], (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          })
        }

        var sqlSelect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" 

        connection.query(sqlSelect, [req.params.id], function(err, result) {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } res.json({
            status: 'Success update data id ' + [req.params.id],
            data: result[0]
          })
        })
      })
    })
   }
   catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

// Delete data
  async delete(req, res) {
    try {

      var sqlDelete = 'DELETE FROM transaksi t WHERE id = ?'

      connection.query(sqlDelete, [req.params.id], (err, result) => {
        if (err){
          res.json({
            status: "Error",
            error: err
          })
        }
        res.json({
          status: 'Success delete data id ' + [req.params.id]
        })
      })
    }
    catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
}


module.exports = new TransaksiController