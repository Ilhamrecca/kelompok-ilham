const express = require('express') // import express
const router = express.Router()
const PemasokController = require('../controllers/pemasokController.js')

router.get('/', PemasokController.getAll) // if acessing localhost:3000/Pemasok, it will do function getAll() in PemasokController class
router.get('/:id', PemasokController.getOne) // if acessing localhost:3000/Pemasok/:id, it will do function getOne() in PemasokController class
router.post('/create', PemasokController.create) // if acessing localhost:3000/create, it will do function create() in PemasokController class
router.put('/update/:id', PemasokController.update) // if acessing localhost:3000/update/:id, it will do function update() in PemasokController class
router.delete('/delete/:id', PemasokController.delete) // if acessing localhost:3000/delete/:id, it will do function delete() in PemasokController class

module.exports = router // expost module