const express = require("express"); // Import express
const pelangganController = require("../controllers/pelangganController.js");
const pelangganValidator = require("../middlewares/validators/pelangganValidator.js");
const router = express.Router(); // Make router from app

router.get("/", pelangganController.getAll); // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get("/:id", pelangganController.getOne); // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post("/create", pelangganValidator.create, pelangganController.create); // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put(
	"/update/:id",
	pelangganValidator.update,
	pelangganController.update
); // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete("/delete/:id", pelangganController.delete); // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
