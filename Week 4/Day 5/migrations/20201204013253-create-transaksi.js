"use strict";
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("Transaksi", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			id_barang: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			id_pelanggan: {
				type: Sequelize.INTEGER,
				allowNull: false,
			},
			jumlah: {
				type: Sequelize.DECIMAL,
				allowNull: false,
			},
			total: {
				type: Sequelize.DECIMAL,
				allowNull: false,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			deletedAt: {
				allowNull: true,
				type: Sequelize.DATE,
			},
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("Transaksi");
	},
};
