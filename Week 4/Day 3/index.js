const express = require('express') // Import Express
const app = express() // Create app from express
const transaksiRoutes = require('./routes/transaksiRoutes.js') // Import transaksiRoutes
const barangRoutes = require('./routes/barangRoutes.js') // Import barangRoutes
const pemasokRoutes = require('./routes/pemasokRoutes.js') // Import transaksiRoutes
const pelangganRoutes = require('./routes/pelangganRoutes.js') // Import barangRoutes

app.use(express.urlencoded({extended: false}))
app.use('/transaksi', transaksiRoutes) // If accessing localhost:3000/transaksi/*, it will use transaksiRoutes
app.use('/barang', barangRoutes) // If accessing localhost:3000/barang/*, it will use barangRoutes
app.use('/pemasok', pemasokRoutes) // If accessing localhost:3000/barang/*, it will use barangRoutes
app.use('/pelanggan', pelangganRoutes) // If accessing localhost:3000/barang/*, it will use barangRoutes

app.listen(3000) // make application have port 3000
