const connection = require("../models/connection.js");

class BarangController {
	// Function getAll barang table
	async getAll(req, res) {
		try {
			var sql = "SELECT * FROM barang";
			// Run query
			connection.query(sql, function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result,
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Function getOne barang table
	async getOne(req, res) {
		try {
			var sql = "SELECT * FROM barang WHERE id = ?"; // make an query varible

			// Run query
			connection.query(sql, [req.params.id], function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result[0],
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Create data
	async create(req, res) {
		try {
			connection.query(
				"INSERT INTO barang(nama, harga, id_pemasok) VALUES (?, ?, ?)",
				[req.body.nama, req.body.harga, req.body.id_pemasok],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					// res.json({
					// 	status: "Success",
					// 	data: result,
					// });
				}
			);
			connection.query(
				"SELECT * FROM barang ORDER BY id DESC LIMIT 1",
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success add Data",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Update data
	async update(req, res) {
		try {
			connection.query(
				"UPDATE barang SET id = ?, nama = ?, harga = ?, id_pemasok = ? WHERE id = ?",
				[
					req.body.id,
					req.body.nama,
					req.body.harga,
					req.body.id_pemasok,
					req.params.id,
				],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Delete data
	async delete(req, res) {
		try {
			connection.query(
				"DELETE FROM barang WHERE id = ?",
				[req.params.id],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}
}

module.exports = new BarangController();
