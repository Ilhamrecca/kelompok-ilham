const connection = require("../models/connection.js");

class TransaksiController {
	// Function getAll transaksi table
	async getAll(req, res) {
		try {
			var sql =
				"SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id"; // make an query varible

			// Run query
			connection.query(sql, function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result,
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Function getOne transaksi table
	async getOne(req, res) {
		try {
			var sql =
				"SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?"; // make an query varible

			// Run query
			connection.query(sql, [req.params.id], function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result[0],
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Create data
	async create(req, res) {
		try {
			connection.query(
				"INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)",
				[
					req.body.id_barang,
					req.body.id_pelanggan,
					req.body.jumlah,
					req.body.total,
				],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					// res.json({
					// 	status: "Success",
					// 	data: result,
					// });
				}
			);
			connection.query(
				"SELECT * FROM transaksi ORDER BY id DESC LIMIT 1",
				[req.body.id_barang],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success add Data",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Update data
	async update(req, res) {
		try {
			connection.query(
				"UPDATE transaksi t SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ? WHERE id = ?",
				[
					req.body.id_barang,
					req.body.id_pelanggan,
					req.body.jumlah,
					req.body.total,
					req.params.id,
				],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Delete data
	async delete(req, res) {
		try {
			connection.query(
				"DELETE FROM transaksi t WHERE id = ?",
				[req.params.id],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}
}

module.exports = new TransaksiController();
