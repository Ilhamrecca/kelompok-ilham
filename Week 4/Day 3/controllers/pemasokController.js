const connection = require("../models/connection.js");

class PemasokController {
	// Function getAll barang table
	async getAll(req, res) {
		try {
			var sql = "SELECT * FROM pemasok";
			// Run query
			connection.query(sql, function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result,
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Function getOne barang table
	async getOne(req, res) {
		try {
			var sql = "SELECT * FROM pemasok WHERE id = ?"; // make an query varible

			// Run query
			connection.query(sql, [req.params.id], function (err, result) {
				if (err) throw err; // If error

				// If success it will return JSON of result
				res.json({
					status: "success",
					data: result[0],
				});
			});
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Create data
	async create(req, res) {
		try {
			connection.query(
				"INSERT INTO pemasok(nama) VALUES (?)",
				[req.body.nama],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
				}
			);
			connection.query(
				"SELECT * FROM pemasok ORDER BY id DESC LIMIT 1",
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success add Data",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Update data
	async update(req, res) {
		try {
			connection.query(
				"UPDATE pemasok SET id = ?, nama = ? WHERE id = ?",
				[req.body.id, req.body.nama, req.params.id],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}

	// Delete data
	async delete(req, res) {
		try {
			connection.query(
				"DELETE FROM pemasok WHERE id = ?",
				[req.params.id],
				(err, result) => {
					if (err) throw err; // If error

					// If success it will return JSON of result
					res.json({
						status: "Success",
						data: result,
					});
				}
			);
		} catch (e) {
			// If error will be send Error JSON
			res.json({
				status: "Error",
			});
		}
	}
}

module.exports = new PemasokController();
