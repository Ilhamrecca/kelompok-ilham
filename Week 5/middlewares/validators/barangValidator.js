const { check, validationResult, matchedData, sanitize } = require('express-validator')
const { ObjectId } = require('mongodb')
const client = require('../../models/connection.js')

module.exports = {
    create : [
        check('id_pemasok').custom(value => {
            return client.db('penjualan').collection('pemasok').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID pemasok doesn't exist")
                }
            })
        }),

        check('nama').isString().notEmpty(),

        check('harga').isNumeric().notEmpty(), 
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID barang doesn't exist")
                }
            })
        }),

        check('id_pemasok').custom(value => {
            return client.db('penjualan').collection('pemasok').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID pemasok doesn't exist")
                }
            })
        }),

        check('nama').isString().notEmpty(),

        check('harga').isNumeric().notEmpty(), 
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}