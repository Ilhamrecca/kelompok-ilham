const { ObjectId } = require("mongodb");
const client = require("../models/connection.js");

class BarangController {
	async getAll(req, res) {
		const penjualan = client.db("penjualan");
		const barang = penjualan.collection("barang");

		barang
			.find({})
			.toArray()
			.then((result) => {
				res.json({
					status: "success",
					data: result,
				});
			});
	}

	async getOne(req, res) {
		const penjualan = client.db("penjualan");
		const barang = penjualan.collection("barang");

		barang
			.findOne({
				_id: new ObjectId(req.params.id),
			})
			.then((result) => {
				res.json({
					status: "success",
					data: result,
				});
			});
	}

	async create(req, res) {
		const penjualan = client.db("penjualan");
		const barang = penjualan.collection("barang");

		const pemasok = await penjualan.collection("pemasok").findOne({
			_id: new ObjectId(req.body.id_pemasok),
		});

		barang
			.insertOne({
				nama: req.body.nama,
				harga: req.body.harga,
				pemasok: pemasok,
			})
			.then((result) => {
				res.json({
					status: "new data added to table barang",
					data: result.ops[0],
				});
			});
	}

	async update(req, res) {
		const penjualan = client.db("penjualan");
		const barang = penjualan.collection("barang");

		const pemasok = await penjualan.collection("pemasok").findOne({
			_id: new ObjectId(req.body.id_pemasok),
		});

		barang
			.updateOne(
				{
					_id: new ObjectId(req.params.id),
				},
				{
					$set: {
						nama: req.body.nama,
						harga: req.body.harga,
						pemasok: pemasok,
					},
				}
			)
			.then(() => {
				return barang.findOne({
					_id: new ObjectId(req.params.id),
				});
			})
			.then((result) => {
				res.json({
					status: "table barang's data have been sucessfully updated",
					data: result,
				});
			});
	}

	async delete(req, res) {
		const penjualan = client.db("penjualan");
		const barang = penjualan.collection("barang");

		barang
			.deleteOne({
				_id: new ObjectId(req.params.id),
			})
			.then((result) => {
				res.json({
					status:
						"one of table barang's data have been successfully deleted",
				});
			});
	}
}

module.exports = new BarangController();
