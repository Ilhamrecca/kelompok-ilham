const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class PemasokController {

    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.insertOne({
            nama: req.body.nama,
        }).then(result => {
            res.json({
                status: "new data added to table pemasok",
                data: result.ops[0]
            })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                nama: req.body.nama
            }
        }).then(() => {
            return pemasok.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "table pemasok's data have been sucessfully updated",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "one of table pemasok's data have been successfully deleted",
            })
        })
    }
}

module.exports = new PemasokController