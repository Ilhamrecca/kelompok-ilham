const express = require('express') // Import express
const app = express() // Make express app
const bodyParser = require('body-parser') // import bodyParser to read post body
const transaksiRoutes = require('./routes/transaksiRoutes') // Import routes for transaksi
const pelangganRoutes = require('./routes/pelangganRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')

//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

app.use('/transaksi', transaksiRoutes) // If accessing localhost:3000/transaksi/*, it will go to transaksiRoutes variable
app.use('/pelanggan', pelangganRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/barang', barangRoutes)

app.listen(3000) // make port 3000 for this app